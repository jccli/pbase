﻿using System;
using System.Globalization;

namespace PostVenta.Web.Infraestructura.Infraestructura
{
    public class PresentadoresHelpers
    {
        public static readonly string MessageToken = "Message";
        public static readonly string SavedMessage = "Guardado con éxito";

        public static string DigitoVerificador(Int32 rut)
        {
            double T = rut;
            double M = 0, S = 1;
            while (T != 0)
            {
                S = (S + T % 10 * (9 - M++ % 6)) % 11;
                T = Math.Floor(T / 10);
            }
            string dv = S != 0 ? (S - 1).ToString() : "K";
            return dv;
        }

        public static string Rut(int rut)
        {
            return $"{Formatear(rut.ToString())}-{DigitoVerificador(rut)}";
        }

        public static bool RutFormateadoEsValido(string rut)
        {
            rut = rut?.ToUpper();
            return string.IsNullOrEmpty(rut) || rut.EndsWith(DigitoVerificador(ExtraerRutDeRutFormateado(rut).GetValueOrDefault()));
        }

        public static int? ExtraerRutDeRutFormateado(string rutFormateado)
        {
            if (string.IsNullOrEmpty(rutFormateado)) return null;

            rutFormateado = rutFormateado.ToUpper();
            rutFormateado = rutFormateado.Replace(",", "");
            rutFormateado = rutFormateado.Replace(".", "");

            if (rutFormateado.Contains("-"))
            {
                rutFormateado = rutFormateado.Replace("-", "");
                rutFormateado = rutFormateado.Substring(0, rutFormateado.Length - 1);
            }
            if (int.TryParse(rutFormateado, out var resultado))
                return resultado;

            return null;
        }

        public static string Formatear(string rut)
        {
            int cont = 0;
            var format = string.Empty;
            if (rut.Length != 0)
            {
                for (int i = rut.Length - 1; i >= 0; i--)
                {
                    format = rut.Substring(i, 1) + format;
                    cont++;
                    if (cont == 3 && i != 0)
                    {
                        format = "." + format;
                        cont = 0;
                    }
                }
            }
            return format;
        }


        public static decimal? ParsearADecimal(string monto)
        {
            if (string.IsNullOrEmpty(monto))
                return 0;

            if (monto.Contains(","))
                return decimal.Parse(monto, new NumberFormatInfo() { NumberDecimalSeparator = "," });

            if (monto.Contains("."))
                return decimal.Parse(monto, new NumberFormatInfo() { NumberDecimalSeparator = "." });

            return decimal.Parse(monto);
        }

        public static string FormatearDeDecimal(decimal? monto)
        {
            return monto.HasValue ? monto.ToString().Replace(",", ".") : "0";
        }

        public static string FormatearAInt(string valor)
        {
            if (string.IsNullOrEmpty(valor)) return string.Empty;
            if (!int.TryParse(valor, out _)) return valor;

            NumberFormatInfo nfi = (NumberFormatInfo)CultureInfo.InvariantCulture.NumberFormat.Clone();
            nfi.NumberGroupSeparator = " ";
            nfi.NumberDecimalDigits = 0;
            return $"{int.Parse(valor).ToString("n", nfi)}";
        }
    }
}