﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;

namespace PostVenta.Web.Infraestructura.Infraestructura
{
    public class RutAttribute : ValidationAttribute, IClientModelValidator
    {
        private string GetErrorMessage()
        {
            return "Rut no válido";
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var entidad = (IRutViewModel)validationContext.ObjectInstance;

            if (string.IsNullOrEmpty(entidad.Rut))
                return ValidationResult.Success;

            return PresentadoresHelpers.RutFormateadoEsValido(entidad.Rut) ? ValidationResult.Success : new ValidationResult(GetErrorMessage());
        }

        public void AddValidation(ClientModelValidationContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }
            ValidationHelper.MergeAttribute(context.Attributes, "data-val", "true");
            ValidationHelper.MergeAttribute(context.Attributes, "data-val-rut", GetErrorMessage());
        }
    }

    public interface IRutViewModel
    {
        string Rut { get; set; }
    }
}
