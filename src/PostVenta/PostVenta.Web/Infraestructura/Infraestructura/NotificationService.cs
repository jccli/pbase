﻿using System.Threading.Tasks;
using PostVenta.Web.Core.Aplicacion.Interfaces;
using PostVenta.Web.Core.Aplicacion.Notifications.Models;

namespace PostVenta.Web.Infraestructura.Infraestructura
{
    public class NotificationService : INotificationService
    {
        public Task SendAsync(Message message)
        {
            return Task.CompletedTask;
        }
    }
}
