﻿using System.Collections.Generic;

namespace PostVenta.Web.Infraestructura.Infraestructura
{
    public class ValidationHelper
    {
        public const string MensajeRequerido = "Campo requerido";
        public const string MensajeIngreseMontoValido = "Ingrese un monto válido";
        public static bool MergeAttribute(IDictionary<string, string> attributes, string key, string value)
        {
            if (attributes.ContainsKey(key))
            {
                return false;
            }

            attributes.Add(key, value);
            return true;
        }
    }
}
