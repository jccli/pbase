﻿using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using PostVenta.Web.Core.Aplicacion.Inspecciones.Commands.CrearInspeccion;

namespace PostVenta.Web.Infraestructura.StartupExtensions
{
    public static class MvcRazorExtensions
    {
        public static IServiceCollection AddMvcWithRazorOptions(this IServiceCollection services)
        {
            services
                .AddMvc(options =>
                {
                    options.ModelBindingMessageProvider.SetValueIsInvalidAccessor(x => $"El valor '{x}' no es válido");
                    options.ModelBindingMessageProvider.SetValueMustBeANumberAccessor(x => $"{x} es un campo numérico");
                    options.ModelBindingMessageProvider.SetMissingBindRequiredValueAccessor(x => $"'{x}' requerido");
                    options.ModelBindingMessageProvider.SetAttemptedValueIsInvalidAccessor((x, y) => $"Es valor '{x}' no es válido para {y}.");
                    options.ModelBindingMessageProvider.SetMissingKeyOrValueAccessor(() => "Requerido");
                    options.ModelBindingMessageProvider.SetUnknownValueIsInvalidAccessor(x => $"Valor no válido para {x}.");
                    options.ModelBindingMessageProvider.SetValueMustNotBeNullAccessor(x => $"'{x}' no es válido");
                    options.ModelBindingMessageProvider.SetMissingRequestBodyRequiredValueAccessor(() => "request body requerido");
                    options.ModelBindingMessageProvider.SetNonPropertyAttemptedValueIsInvalidAccessor(x => $"'{x}' no es válido");
                    options.ModelBindingMessageProvider.SetNonPropertyUnknownValueIsInvalidAccessor(() => "No válido");
                    options.ModelBindingMessageProvider.SetNonPropertyValueMustBeANumberAccessor(() => "Ingrese un número válido");
                })
                .AddRazorPagesOptions(options =>
                {
                    options.Conventions.AddPageRoute("/Home/Index", "");
                    options.Conventions.AllowAnonymousToPage("/Account/Login");
                    options.Conventions.AuthorizePage("/SignOut");
                    options.Conventions.AuthorizePage("/Descarga");
                    options.Conventions.AuthorizeFolder("/Home");
                    options.Conventions.AuthorizeFolder("/Inspecciones");
                    options.Conventions.AuthorizeFolder("/Asistencias");
                })
               .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
               .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<CrearInspeccionCommandValidacion>());



            return services;
        }
    }
}
