﻿using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Microsoft.AspNetCore.Mvc.TagHelpers;

namespace PostVenta.Web.Infraestructura.StartupExtensions
{
    [HtmlTargetElement("div", Attributes = ValidationForAttributeName)]
    public class ValidationMessageSupportDivTagHelper : ValidationMessageTagHelper
    {
        private const string ValidationForAttributeName = "asp-validation-for";
        public ValidationMessageSupportDivTagHelper(IHtmlGenerator generator) : base(generator)
        {
        }
    }
}
