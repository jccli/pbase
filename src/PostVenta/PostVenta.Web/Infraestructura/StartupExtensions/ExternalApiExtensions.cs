﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PostVenta.Web.Infraestructura.Services;
using System;
using PostVenta.Web.Core.Aplicacion.Interfaces;
using PostVenta.Web.Infraestructura.Services.LoginBanco;

namespace PostVenta.Web.Infraestructura.StartupExtensions
{
  

    public static class ExternalApiExtensions
    {
        public static IServiceCollection AddExternalApis(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<ILoginBanco, LoginBanco>();

            services.AddHttpClient<IHolidayService, HolidayService>(client => 
            {
                client.BaseAddress = new Uri(configuration["External:HolidayApi:UrlBase"]);
            });

            services.AddHttpClient<ICalculatorService, CalculatorService>(client =>
            {
                client.BaseAddress = new Uri(configuration["External:Calculator:UrlBase"]);
            });

            return services;
        }
    }
}
