﻿using Microsoft.AspNetCore.WebUtilities;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace PostVenta.Web.Infraestructura.Services
{
    public class HolidayService : IHolidayService
    {
        private readonly HttpClient _client;

        public HolidayService(HttpClient client)
        {
            _client = client;
        }

        public async Task<string> ListarFeriados()
        {
            var query = new Dictionary<string, string> { ["key"] = "18550581-3abc-45b5-9771-80773714a48e", ["country"] = "PE", ["year"] = "2018" };
            var response = await _client.GetAsync(QueryHelpers.AddQueryString("/v1/holidays", query));
            return await response.Content.ReadAsStringAsync();
        }
    }
}
