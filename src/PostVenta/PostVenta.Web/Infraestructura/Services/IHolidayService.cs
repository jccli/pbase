﻿using System.Threading.Tasks;

namespace PostVenta.Web.Infraestructura.Services
{
    public interface IHolidayService
    {
        Task<string> ListarFeriados();
    }
}