﻿using System.Threading.Tasks;

namespace PostVenta.Web.Infraestructura.Services
{
    public interface ICalculatorService
    {
        Task<string> Sumar();
    }
}