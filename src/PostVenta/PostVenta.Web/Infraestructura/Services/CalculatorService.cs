﻿using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace PostVenta.Web.Infraestructura.Services
{
    public class CalculatorService : ICalculatorService
    {
        private readonly HttpClient _client;

        public CalculatorService(HttpClient client)
        {
            _client = client;
        }

        public async Task<string> Sumar()
        {
            var result = await _client.PostAsync("/calculator.asmx?op=Multiply", new StringContent(PrepareSoapMessage(4, 3), Encoding.UTF8, "text/xml"));
            return await result.Content.ReadAsStringAsync();
        }

        private string PrepareSoapMessage(int a, int b)
        {
            StringBuilder soapRequest = new StringBuilder("<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"");
            soapRequest.Append(" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"");
            soapRequest.Append(" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:Multiply \">");
            soapRequest.Append("<soapenv:Body><Multiply xmlns=\"http://tempuri.org/\">");
            soapRequest.Append($"<intA>{a}</intA>");
            soapRequest.Append($"<intB>{b}</intB>");
            soapRequest.Append("</Multiply></soapenv:Body></soapenv:Envelope>");
            return soapRequest.ToString();
        }
    }
}
