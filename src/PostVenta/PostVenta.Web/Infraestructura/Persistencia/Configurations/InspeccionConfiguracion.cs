﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PostVenta.Web.Core.Dominio.Entities;

namespace PostVenta.Web.Infraestructura.Persistencia.Configurations
{
    public class InspeccionConfiguracion : IEntityTypeConfiguration<Inspeccion>
    {
        public void Configure(EntityTypeBuilder<Inspeccion> builder)
        {
            builder.Property(e => e.Folio).IsRequired().HasMaxLength(10);
            builder.HasIndex(i => i.Folio).IsUnique();
        }
    }
}
