﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PostVenta.Web.Core.Dominio.Entities;

namespace PostVenta.Web.Infraestructura.Persistencia.Configurations
{
    public class VehiculoConfiguracion : IEntityTypeConfiguration<Vehiculo>
    {
        public void Configure(EntityTypeBuilder<Vehiculo> builder)
        {
            builder.Property(e => e.Patente).IsRequired().HasMaxLength(7);
            builder.HasIndex(i => i.Patente).IsUnique();
        }
    }
}
