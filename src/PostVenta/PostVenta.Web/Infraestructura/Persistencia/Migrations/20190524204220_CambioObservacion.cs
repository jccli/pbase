﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PostVenta.Web.Infraestructura.Persistencia.Migrations
{
    public partial class CambioObservacion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Comentario",
                table: "Asistencias",
                newName: "Observacion");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Observacion",
                table: "Asistencias",
                newName: "Comentario");
        }
    }
}
