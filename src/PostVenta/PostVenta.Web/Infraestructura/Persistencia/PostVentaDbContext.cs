﻿using Microsoft.EntityFrameworkCore;
using PostVenta.Web.Core.Aplicacion.Interfaces;
using PostVenta.Web.Core.Dominio.Entities;

namespace PostVenta.Web.Infraestructura.Persistencia
{
    public class PostVentaDbContext : DbContext
    {
        public PostVentaDbContext(DbContextOptions<PostVentaDbContext> options) : base(options) { }

        public DbSet<Vehiculo> Vehiculos { get; set; }
        public DbSet<Inspeccion> Inspecciones { get; set; }

        public DbSet<Archivo> Archivos { get; set; }
        public DbSet<Compania> Companias { get; set; }

        public DbSet<Asistencia> Asistencias { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(PostVentaDbContext).Assembly);
        }
    }
}
