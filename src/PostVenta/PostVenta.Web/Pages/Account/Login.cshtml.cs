﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PostVenta.Web.Core.Aplicacion.Seguridad;

namespace PostVenta.Web.Pages.Account
{
    public class LoginModel : PageModel
    {
        [BindProperty] public LoginViewModel LogInModel { get; set; }

        public void OnGet(string returnUrl)
        {
            LogInModel = new LoginViewModel { ReturnUrl = returnUrl };
        }


        public async Task<IActionResult> OnPostAsync(string returnUrl)
        {
            if (!ModelState.IsValid) return Page();

            var usuario = new Usuario { Id = 1, Login = "jose", Correo = "nocompila@hotmail.com", Nombre = "jose calderon" };

            if (usuario == null)
            {
                ModelState.AddModelError("", "El usuario no existe");
                return Page();
            }

            if (usuario.Contrasena != LogInModel.Password)
            {
                ModelState.AddModelError("", "La contrasena no es válida");
                return Page();
            }


            var identity = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationScheme, ClaimTypes.Name, ClaimTypes.Role);
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, usuario.Correo));
            identity.AddClaim(new Claim(ClaimTypes.Name, usuario.Nombre));
            identity.AddClaim(new Claim("usuarioid", usuario.Id.ToString()));

            //if (usuario.EsAdministrador)
            //    identity.AddClaim(new Claim(ClaimTypes.Role, "Admin"));

            var principal = new ClaimsPrincipal(identity);
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal, new AuthenticationProperties { IsPersistent = true });

            return RedirectToPage("../Home/Index");
        }
    }
}