using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PostVenta.Web.Core.Aplicacion.Inspecciones.Queries.ListarInspecciones;

namespace PostVenta.Web.Pages.Inspecciones
{
    public class IndexModel : PageModel
    {
        private readonly IMediator _mediator;

        public IndexModel(IMediator mediator) => _mediator = mediator;


        public ListarInspeccionesViewModel Vm { get; set; }

        public async Task OnGet()
        {
            Vm = await _mediator.Send(new ListarInspeccionesQuery());
        }
    }
}