using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PostVenta.Web.Core.Aplicacion.Inspecciones.Commands.CrearInspeccion;

namespace PostVenta.Web.Pages.Inspecciones
{
    public class CrearModel : PageModel
    {
        private readonly IMediator _mediator;

        public CrearModel(IMediator mediator) => _mediator = mediator;
        [BindProperty] public CrearInspeccionCommand Vm { get; set; }
        public void OnGet()
        {
            Vm = new CrearInspeccionCommand();
        }

        public async Task<IActionResult> OnPost()
        {
            await _mediator.Send(Vm);
            return RedirectToPage("Index", new { msg = "done" });
        }
    }
}