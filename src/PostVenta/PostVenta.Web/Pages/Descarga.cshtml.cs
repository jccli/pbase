using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PostVenta.Web.Infraestructura.Persistencia;

namespace PostVenta.Web.Pages
{
    public class DescargaModel : PageModel
    {
        private readonly PostVentaDbContext _db;
        public DescargaModel(PostVentaDbContext db)
        {
            _db = db;
        }
        public async Task<IActionResult> OnGet(int id)
        {
            var archivo = _db.Archivos.Find(id);
            return File(archivo.ArchivoBytes, archivo.Extension, archivo.NombreArchivo);
        }
    }
}