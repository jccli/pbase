using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PostVenta.Web.Core.Aplicacion.Inspecciones.Queries.ListarInspecciones;
using PostVenta.Web.Infraestructura.Services;

namespace PostVenta.Web.Pages.Home
{
    public class IndexModel : PageModel
    {
        private readonly IHolidayService _holidayService;
        private readonly ICalculatorService _calculatorService;

        public IndexModel(IHolidayService holidayService, ICalculatorService calculatorService)
        {
            _holidayService = holidayService;
            _calculatorService = calculatorService;
        }


        public List<InspeccionDto> Categories { get; set; }

        public async Task OnGet()
        {
            Categories = new List<InspeccionDto>();
            var restResponse = await _holidayService.ListarFeriados();
            var soapResponse = await _calculatorService.Sumar();
        }
    }
}