﻿using System.Threading.Tasks;
using PostVenta.Web.Core.Aplicacion.Notifications.Models;

namespace PostVenta.Web.Core.Aplicacion.Interfaces
{
    public interface INotificationService
    {
        Task SendAsync(Message message);
    }
}
