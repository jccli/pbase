﻿using AutoMapper;

namespace PostVenta.Web.Core.Aplicacion.Interfaces.Mapping
{
    public interface IHaveCustomMapping
    {
        void CreateMappings(Profile configuration);
    }
}
