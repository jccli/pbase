﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PostVenta.Web.Core.Aplicacion.Interfaces
{
    public interface ILoginBanco
    {
        bool Autenticar(string usuario, string clave);
    }
}
