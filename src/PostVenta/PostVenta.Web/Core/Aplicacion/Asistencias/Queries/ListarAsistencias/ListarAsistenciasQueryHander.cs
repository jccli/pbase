﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using PostVenta.Web.Infraestructura.Persistencia;

namespace PostVenta.Web.Core.Aplicacion.Asistencias.Queries.ListarAsistencias
{
    public class ListarAsistenciasQueryHander : IRequestHandler<ListarAsistenciasQuery, ListarAsistenciasViewModel>
    {
        private readonly PostVentaDbContext _context;
        private readonly IMapper _mapper;

        public ListarAsistenciasQueryHander(PostVentaDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ListarAsistenciasViewModel> Handle(ListarAsistenciasQuery request, CancellationToken cancellationToken)
        {
            var asistencias = await _context.Asistencias.OrderBy(p => p.Id).ToListAsync(cancellationToken);

            var model = new ListarAsistenciasViewModel
            {
                Asistencias = _mapper.Map<IEnumerable<AsistenciaDto>>(asistencias),
                PuedeCrear = true 
            };
            return model;
        }
    }
}
