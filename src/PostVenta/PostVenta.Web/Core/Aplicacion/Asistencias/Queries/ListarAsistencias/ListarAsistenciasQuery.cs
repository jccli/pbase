﻿using MediatR;
using PostVenta.Web.Core.Aplicacion.Asistencias.Queries.ListarAsistencias;

namespace PostVenta.Web.Core.Aplicacion.Asistencias.Queries.ListarAsistencias
{
    public class ListarAsistenciasQuery : IRequest<ListarAsistenciasViewModel>
    {
    }
}
