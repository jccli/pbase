﻿using AutoMapper;
using PostVenta.Web.Core.Aplicacion.Interfaces.Mapping;
using PostVenta.Web.Core.Dominio.Entities;
using PostVenta.Web.Infraestructura.Infraestructura;
using System;

namespace PostVenta.Web.Core.Aplicacion.Asistencias.Queries.ListarAsistencias
{
    public class AsistenciaDto 
    {
        public int Id { get; set; }
        public DateTime FechaRegistro { get; set; }
        public string Observacion { get; set; }
    }
}