﻿using System.Collections.Generic;

namespace PostVenta.Web.Core.Aplicacion.Asistencias.Queries.ListarAsistencias
{
    public class ListarAsistenciasViewModel
    {
        public IEnumerable<AsistenciaDto> Asistencias { get; set; }

        public bool PuedeCrear { get; set; }
    }
}
