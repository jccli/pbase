﻿using AutoMapper;
using PostVenta.Web.Core.Aplicacion.Interfaces.Mapping;
using PostVenta.Web.Core.Dominio.Entities;
using PostVenta.Web.Infraestructura.Infraestructura;
using System;

namespace PostVenta.Web.Core.Aplicacion.Inspecciones.Queries.ListarInspecciones
{
    public class InspeccionDto : IHaveCustomMapping
    {
        public int Id { get; set; }

        public string Folio { get; set; }

        public string VehiculoPatente { get; set; }

        public DateTime FechaInspeccion { get; set; }


        public int? ArchivoId { get; set; }

        public string TipoEstado { get; set; }


        public string RutConductor { get; set; }

        public string Observacion { get; set; }
        public string Comentario { get; set; }


        public void CreateMappings(Profile configuration)
        {
            configuration.CreateMap<Inspeccion, InspeccionDto>()
                .ForMember(pDTO => pDTO.TipoEstado, opt => opt.MapFrom(p => p.TipoEstado.ToString()))
                .ForMember(pDTO => pDTO.RutConductor, opt => opt.MapFrom(p => p.RutConductor.HasValue ? PresentadoresHelpers.Rut(p.RutConductor.Value) : ""))
                .ForMember(pDTO => pDTO.VehiculoPatente, opt => opt.MapFrom(p => p.Vehiculo != null ? p.Vehiculo.Patente : "SINPAT"));
        }
    }
}