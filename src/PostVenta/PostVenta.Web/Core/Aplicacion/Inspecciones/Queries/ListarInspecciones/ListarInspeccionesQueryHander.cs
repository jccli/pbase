﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using PostVenta.Web.Infraestructura.Persistencia;

namespace PostVenta.Web.Core.Aplicacion.Inspecciones.Queries.ListarInspecciones
{
    public class ListarInspeccionesQueryHander : IRequestHandler<ListarInspeccionesQuery, ListarInspeccionesViewModel>
    {
        private readonly PostVentaDbContext _context;
        private readonly IMapper _mapper;

        public ListarInspeccionesQueryHander(PostVentaDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ListarInspeccionesViewModel> Handle(ListarInspeccionesQuery request, CancellationToken cancellationToken)
        {
            var products = await _context.Inspecciones.OrderBy(p => p.Folio).Include(p => p.Vehiculo).ToListAsync(cancellationToken);

            var model = new ListarInspeccionesViewModel
            {
                Inspecciones = _mapper.Map<IEnumerable<InspeccionDto>>(products),
                PuedeCrear = true //si cumple algun
            };
            return model;
        }
    }
}
