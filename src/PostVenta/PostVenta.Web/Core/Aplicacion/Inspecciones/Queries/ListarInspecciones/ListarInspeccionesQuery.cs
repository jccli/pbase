﻿using MediatR;

namespace PostVenta.Web.Core.Aplicacion.Inspecciones.Queries.ListarInspecciones
{
    public class ListarInspeccionesQuery : IRequest<ListarInspeccionesViewModel>
    {
    }
}
