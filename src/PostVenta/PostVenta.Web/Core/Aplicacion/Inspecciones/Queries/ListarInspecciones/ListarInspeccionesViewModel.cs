﻿using System.Collections.Generic;

namespace PostVenta.Web.Core.Aplicacion.Inspecciones.Queries.ListarInspecciones
{
    public class ListarInspeccionesViewModel
    {
        public IEnumerable<InspeccionDto> Inspecciones { get; set; }

        public bool PuedeCrear { get; set; }
    }
}
