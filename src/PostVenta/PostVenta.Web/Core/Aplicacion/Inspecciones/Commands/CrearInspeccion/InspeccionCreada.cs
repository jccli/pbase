﻿using MediatR;
using PostVenta.Web.Core.Aplicacion.Interfaces;
using PostVenta.Web.Core.Aplicacion.Notifications.Models;
using System.Threading;
using System.Threading.Tasks;

namespace PostVenta.Web.Core.Aplicacion.Inspecciones.Commands.CrearInspeccion
{
    public class InspeccionCreada : INotification
    {
        public string Id { get; set; }

        public class CustomerCreatedHandler : INotificationHandler<InspeccionCreada>
        {
            private readonly INotificationService _notification;

            public CustomerCreatedHandler(INotificationService notification)
            {
                _notification = notification;
            }

            public async Task Handle(InspeccionCreada notification, CancellationToken cancellationToken)
            {
                await _notification.SendAsync(new Message());
            }
        }
    }
}
