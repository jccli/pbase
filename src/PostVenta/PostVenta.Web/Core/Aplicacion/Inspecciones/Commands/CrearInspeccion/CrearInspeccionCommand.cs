﻿using MediatR;
using PostVenta.Web.Core.Aplicacion.Interfaces;
using PostVenta.Web.Core.Dominio.Entities;
using PostVenta.Web.Core.Dominio.Tipos;
using PostVenta.Web.Infraestructura.Infraestructura;
using System;
using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.IO;
using PostVenta.Web.Infraestructura.Persistencia;

namespace PostVenta.Web.Core.Aplicacion.Inspecciones.Commands.CrearInspeccion
{
    public class CrearInspeccionCommand : IRequest, IRutViewModel
    {
        public CrearInspeccionCommand()
        {
            FechaInspeccion = DateTime.Now;
        }

        public string Id { get; set; }


        [Required]
        public DateTime FechaInspeccion { get; set; }

        [Required]
        public string Folio { get; set; }

        [Required]
        public string VehiculoPatente { get; set; }


        [Required(ErrorMessage = "Rquerido")]
        [Display(Name = "Adjuntar arcihvo")]
        public IFormFile UploadArchivo { get; set; }

        public TiposEstado TipoEstado { get; set; }

        public int? CompaniaId { get; set; }

        [Rut]
        [Required]
        public string Rut { get; set; }

        [Nombre]
        [Required]
        public string Observacion { get; set; }

        public string Comentario { get; set; }





        public class Handler : IRequestHandler<CrearInspeccionCommand, Unit>
        {
            private readonly PostVentaDbContext _context;
            private readonly IMediator _mediator;

            public Handler(PostVentaDbContext context, IMediator mediator)
            {
                _context = context;
                _mediator = mediator;
            }

            public async Task<Unit> Handle(CrearInspeccionCommand request, CancellationToken cancellationToken)
            {

                MemoryStream ms = new MemoryStream();
                request.UploadArchivo.CopyTo(ms);

                var newArchivo = new Archivo
                {
                    ArchivoBytes = ms.ToArray(),
                    NombreArchivo = request.UploadArchivo.FileName,
                    Extension = request.UploadArchivo.ContentType
                };
                await _context.Archivos.AddAsync(newArchivo);
                await _context.SaveChangesAsync(cancellationToken);

                var entity = new Inspeccion
                {
                    ArchivoId = newArchivo.Id,
                    CompaniaId = request.CompaniaId,
                    RutConductor = PresentadoresHelpers.ExtraerRutDeRutFormateado(request.Rut),
                    TipoEstado = request.TipoEstado,
                    Observacion = request.Observacion,
                    Comentario = request.Comentario,
                    FechaInspeccion = request.FechaInspeccion,
                    Folio = request.Folio,
                    Vehiculo = new Vehiculo
                    {
                        Patente = request.VehiculoPatente
                    }
                };

                _context.Inspecciones.Add(entity);
                await _context.SaveChangesAsync(cancellationToken);
                await _mediator.Publish(new InspeccionCreada { Id = entity.Id.ToString() }, cancellationToken);
                return Unit.Value;
            }
        }
    }
}
