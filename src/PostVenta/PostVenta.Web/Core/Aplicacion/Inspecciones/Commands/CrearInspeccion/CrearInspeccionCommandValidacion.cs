﻿using FluentValidation;

namespace PostVenta.Web.Core.Aplicacion.Inspecciones.Commands.CrearInspeccion
{
    public class CrearInspeccionCommandValidacion : AbstractValidator<CrearInspeccionCommand>
    {
        public CrearInspeccionCommandValidacion()
        {
            RuleFor(x => x.Folio).MaximumLength(6);
        }
    }
}