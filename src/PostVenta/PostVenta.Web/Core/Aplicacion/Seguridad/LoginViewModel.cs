﻿using System.ComponentModel.DataAnnotations;

namespace PostVenta.Web.Core.Aplicacion.Seguridad
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "es requerido")]
        [EmailAddress(ErrorMessage = "no válido")]
        public string Username { get; set; }

        [Required(ErrorMessage = "es requerida")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public bool RememberMe { get; set; }

        public string ReturnUrl { get; set; }
    }
}
