﻿namespace PostVenta.Web.Core.Aplicacion.Seguridad
{
    public class Usuario
    {
        public Usuario()
        {
            Contrasena = "123";
        }
        public int Id { get; set; }
        public string Login { get; set; }
        public string Correo { get; set; }
        public string Nombre { get; set; }

        public string Contrasena { get; set; }
    }
}
