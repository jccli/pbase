﻿namespace PostVenta.Web.Core.Dominio.Tipos
{
    public enum TiposEstado
    {
        Indefinido,
        Aprobado,
        Cancelado,
        Finalizado,
    }
}
