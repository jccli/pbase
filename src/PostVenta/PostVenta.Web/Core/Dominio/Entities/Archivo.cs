﻿namespace PostVenta.Web.Core.Dominio.Entities
{
    public class Archivo
    {
        public int Id { get; set; }
        public byte[] ArchivoBytes { get; set; }
        public string NombreArchivo { get; set; }
        public string Extension { get; set; }
    }
}
