﻿using PostVenta.Web.Core.Dominio.Tipos;
using System;

namespace PostVenta.Web.Core.Dominio.Entities
{
    public class Inspeccion
    {
        public int Id { get; set; }
        public DateTime FechaInspeccion { get; set; }
        public string Folio { get; set; }
        public int VehiculoId { get; set; }
        public Vehiculo Vehiculo { get; set; }

        public int? ArchivoId { get; set; }
        public Archivo Archivo { get; set; }

        public TiposEstado TipoEstado { get; set; }

        public int? CompaniaId { get; set; }
        public Compania Compania { get; set; }

        public int? RutConductor { get; set; }

        public string Observacion { get; set; }
        public string Comentario { get; set; }
    }
}
