﻿namespace PostVenta.Web.Core.Dominio.Entities
{
    public class Compania
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
    }
}
