﻿namespace PostVenta.Web.Core.Dominio.Entities
{
    public class Vehiculo
    {
        public int Id { get; set; }
        public string Patente { get; set; }
    }
}
