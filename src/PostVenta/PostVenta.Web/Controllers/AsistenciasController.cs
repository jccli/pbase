﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PostVenta.Web.Core.Aplicacion.Asistencias.Queries.ListarAsistencias;
using PostVenta.Web.Core.Aplicacion.Inspecciones.Queries.ListarInspecciones;
using PostVenta.Web.Core.Aplicacion.Interfaces;

namespace PostVenta.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AsistenciasController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ILoginBanco _loginBanco;
        public AsistenciasController(IMediator mediator, ILoginBanco loginBanco)
        {
            _mediator = mediator;
            _loginBanco = loginBanco;
        }

        [HttpGet]
        public async Task<ActionResult<ListarInspeccionesViewModel>> Get()
        {
            if (_loginBanco.Autenticar("jose", "asd"))
            {
                string hola = "asd";
            }

            return Ok(await _mediator.Send(new ListarAsistenciasQuery()));
        }
    }
}