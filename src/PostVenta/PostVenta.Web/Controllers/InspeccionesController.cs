﻿using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using PostVenta.Web.Core.Aplicacion.Inspecciones.Queries.ListarInspecciones;

namespace PostVenta.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InspeccionesController : ControllerBase
    {
        private readonly IMediator _mediator;

        public InspeccionesController(IMediator mediator) => _mediator = mediator;

        [HttpGet]
        public async Task<ActionResult<ListarInspeccionesViewModel>> Get()
        {
            return Ok(await _mediator.Send(new ListarInspeccionesQuery()));
        }
    }
}